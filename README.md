# Editer les fichiers de son site web directement sur VS Code

Télécharger, éditer et uploader les fichiers depuis son espace web hébergé est un process qu'on utilise
généralement lorsque nous travaillons sur un projet, peu importe le langage de programmation. Dans cet article, nous allons voir comment nous pouvons directement travailler sur ces fichiers directement sur **VS Code**.

**Prérequis:**

- Une connexion internet
- VS Code
- Un espace web fonctionnel

**NB:** Vous pouvez télécharger VS Code [ici](https://code.visualstudio.com/Download).

## Installation de l'extension ftp-simple

Cliquez sur l'icône carrée sur le côté gauche pour accéder au gestionnaire d'extensions. Une fois que vous y êtes, recherchez simplement **ftp-simple** dans le texte. Installez-la en cliquant sur le bouton d'installation.

![Image of Yaktocat](screenshots/ftp-simple_installation.png)

**NB:** Il y a tellement d'extensions sur le marché pour faire ce type de travail. Pour cet article, j'ai choisi ftp-simple, ma préférée et très simple à utiliser et à configurer.

## Configuration de ftp-simple

Appuyez sur la touche F1 de votre clavier qui ouvre une zone de texte pour les options. Commencez à taper quelque chose comme config, puis sélectionnez l'option Paramètres de connexion ftp dans la liste déroulante.

![Image of Yaktocat](screenshots/ftp-simple_config_option.png)

Le fichier de configuration de ftp-simple s'ouvre pour édition.

![Image of Yaktocat](screenshots/ftp-simple_config_file.png)

**Remarque:** Si cela n'apparaît pas, appuyez simplement sur le bouton de rechargement dans la fenêtre d'extension, ou quittez et ouvrez VS Code..

Le fichier de configuration est au format JSON et pour ceux qui l'ont déjà remarqué, il est possible d'ajouter plusieurs hôtes. Parmis les paramètres de configuration, nous avons:

- **name:** ce nom est utilisé pour identifier un hôte.
- **host:** l'adresse du serveur FTP.
- **port:** le port utilisé pour la connexion. Généralement 21 ou 20.
- **type:** sftp ou ftp.
- **username:** le nom d'utilisateur utilisé pour la connexion.
- **password:** le mot de passe utilisé pour la connexion.
- **path:** le répertoir à ouvrir.
- **autosave:** sauvegarde automatique.
- **confirm:** confirmer la modification.

Un exemple de configuration:

![Image of Yaktocat](screenshots/ftp-simple_config_file_exemple.png)

## Edition des fichiers

Appuyez à nouveau sur la touche **F1** pour afficher la zone de recherche des options et tapez **remote** à l'intérieur. Il affichera **Remote directory open to workspace**, sélectionnez cette option.

![Image of Yaktocat](screenshots/ftp-simple_remote_open.png)

Lorsque vous sélectionnez l'option **Remote directory open to workspace**, le nom de la connexion (que vous avez enregistré dans la configuration json) apparaîtra ainsi que le chemin dans les écrans suivants.

![Image of Yaktocat](screenshots/ftp-simple_remote_test.png)

![Image of Yaktocat](screenshots/ftp-simple_remote_test_browse.png)

Ça y est, vous êtes prêt à utiliser les fichiers distants dans l'éditeur de code Visual Studio. Vous pouvez voir les fichiers distants dans le volet gauche de l'éditeur.

![Image of Yaktocat](screenshots/tada.png)

TADA!
